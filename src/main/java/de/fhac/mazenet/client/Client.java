package de.fhac.mazenet.client;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.bind.UnmarshalException;

import de.fhac.mazenet.client.game.PinPosition;
import de.fhac.mazenet.client.game.PinToShift;
import de.fhac.mazenet.client.game.Shifts;
import de.fhac.mazenet.server.game.*;
import de.fhac.mazenet.server.generated.*;
import de.fhac.mazenet.server.networking.*;

public class Client {
	private static  Socket clientsocket;
	private static  XmlOutputStream outToServer;
	private static  XmlInputStream inFromServer;
	private static  int playerId;
	private static  PinPosition pinpos = new PinPosition(0,0);
	private static  Treasure t;
	private static boolean finish = false;
	private static Shifts  touslesshifts  ;
	private static PositionData verbotenpos = new PositionData();
	
	public static void main(String[]args) {
		try {
			var sc1 = new Scanner(System.in);
			clientsocket = new Socket("127.0.0.1", sc1.nextInt());// verbindet der Client zu dem Server
			inFromServer = new XmlInputStream(clientsocket.getInputStream());
			outToServer = new XmlOutputStream(clientsocket.getOutputStream());
			sc1.nextLine();
			System.out.println("Name");
			MazeCom mazecom = MazeComMessageFactory.createLoginMessage(sc1.nextLine());
			outToServer.write(mazecom);
			MazeCom maze;
			while (finish == false) {
				maze = inFromServer.readMazeCom();
				touslesshifts = new Shifts();
				gerermazecom(maze);
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// TODO Auto-generated catch block
		} catch (UnmarshalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// Mazecom Messages verwalten
	public static void gerermazecom(MazeCom maze) {
		switch (maze.getMessagetype()) {
		case LOGINREPLY:
			playerId = maze.getLoginReplyMessage().getNewID();
			break;
		case AWAITMOVE:
			System.out.println(maze.getMessagetype());
			Board clientBoard = new Board(maze.getAwaitMoveMessage().getBoard());
			MazeCom maze1 = new MazeCom();
			t=maze.getAwaitMoveMessage().getTreasureToFindNext();
			MoveMessageData Move=createMoveMessage(clientBoard.getShiftCard(), clientBoard);
			pinpos.setPinpos(Move.getNewPinPos());
			maze1.setMoveMessage(Move);
			maze1.setMessagetype(MazeComMessagetype.MOVE);
			try {
				outToServer.write(maze1);	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case WIN:
			finish = true;
		default:
			break;
		}
	}
	//treasurepos finden
	public static  PositionData TrouverlaPos(BoardData board, Treasure treasure) {
		PositionData treasurepos = new PositionData();
		for (int i = 0; i < board.getRow().size(); i++) 
		{
			for (int j = 0; j < board.getRow().get(i).getCol().size(); j++) 
			{
				if (board.getRow().get(i).getCol().get(j).getTreasure() == treasure)
				{
					treasurepos.setCol(j);
					treasurepos.setRow(i);
					return treasurepos;
				}
			}
		}
		return treasurepos;
	}
	// alle Positionen finden
	public static  List<PositionData> ReachablePositionsfinden(BoardData board, int i, int j, boolean[][] Visited_test,List<PositionData> allpositions) {
		if (board.getRow().get(i).getCol().get(j).getOpenings().isBottom()) {
			if (i < 6 && board.getRow().get(i + 1).getCol().get(j).getOpenings().isTop() == true
					&& Visited_test[i + 1][j] == false) {
				Visited_test[i + 1][j] = true;
				PositionData newpos = new PositionData();
				newpos.setRow(i);
				newpos.setCol(j);
				allpositions.add(newpos);
				ReachablePositionsfinden(board, i + 1, j, Visited_test, allpositions);
			}
		}
		// überprüfen,ob es ein Zugang zur linken Karte gibt
		if (board.getRow().get(i).getCol().get(j).getOpenings().isLeft()) {
			if (j > 0 && board.getRow().get(i).getCol().get(j - 1).getOpenings().isRight()
					&& Visited_test[i][j - 1] == false) {
				Visited_test[i][j - 1] = true;
				PositionData newpos = new PositionData();
				newpos.setRow(i);
				newpos.setCol(j);
				allpositions.add(newpos);
				ReachablePositionsfinden(board, i, j - 1, Visited_test, allpositions);
			}
		}
		// überprüfen,ob es ein Zugang zur rechten Karte gibt
		if (board.getRow().get(i).getCol().get(j).getOpenings().isRight()) {
			if (j < 6 && board.getRow().get(i).getCol().get(j + 1).getOpenings().isLeft()
					&& Visited_test[i][j + 1] == false) {
				Visited_test[i][j + 1] = true;
				PositionData newpos = new PositionData();
				newpos.setRow(i);
				newpos.setCol(j);
				allpositions.add(newpos);
				ReachablePositionsfinden(board, i, j + 1, Visited_test, allpositions);
			}
			// überprüfen,ob es ein Zugang zur obenen Karte gibt
					}
			if (board.getRow().get(i).getCol().get(j).getOpenings().isTop()) {
				if (i > 0 && board.getRow().get(i - 1).getCol().get(j).getOpenings().isBottom()
						&& Visited_test[i - 1][j] == false) {
					Visited_test[i - 1][j] = true;
					PositionData newpos = new PositionData();
					newpos.setRow(i);
					newpos.setCol(j);
					allpositions.add(newpos);
					ReachablePositionsfinden(board, i - 1, j, Visited_test, allpositions);
				}
			}
		PositionData newpos = new PositionData();
		newpos.setRow(i);
		newpos.setCol(j);
		allpositions.add(newpos);
		return allpositions;
	}

	// Board Shiften und ein neues Board erzeugen
	public static  BoardData lefakeshift(PositionData shiftpos, BoardData board) {
		Board fakeboard = new Board(board);
		CardData newCard, replacementCard;	
		if (shiftpos.getCol() == 0)// horizontal
		{
			newCard = fakeboard.getRow().get(shiftpos.getRow()).getCol().get(6);
			for (int i = 6; i > 0; i--) {
				replacementCard = fakeboard.getRow().get(shiftpos.getRow()).getCol().get(i - 1);
				fakeboard.getRow().get(shiftpos.getRow()).getCol().set(i, replacementCard);
			}
		}
		else if (shiftpos.getCol() == 6)// horizontal schieb
		{
			newCard = fakeboard.getRow().get(shiftpos.getRow()).getCol().get(0);
			for (int i = 0; i < 6; i++) {
				replacementCard = fakeboard.getRow().get(shiftpos.getRow()).getCol().get(i + 1);
				fakeboard.getRow().get(shiftpos.getRow()).getCol().set(i, replacementCard);
			}
		}
		else if (shiftpos.getRow() == 0)// vertical
		{
			newCard = fakeboard.getRow().get(6).getCol().get(shiftpos.getCol());
			for (int i = 6; i >= 1; i--) {		
				replacementCard = fakeboard.getRow().get(i-1).getCol().get(shiftpos.getCol());
				fakeboard.getRow().get(i).getCol().set(shiftpos.getCol(), replacementCard);
			}
		}
		else	// vertical
		{
			newCard = fakeboard.getRow().get(0).getCol().get(shiftpos.getCol());
			for (int i = 0; i < 6; i++) {				
				replacementCard = fakeboard.getRow().get(i + 1).getCol().get(shiftpos.getCol());
				fakeboard.getRow().get(i).getCol().set(shiftpos.getCol(), replacementCard);
			}
		}
		fakeboard.getRow().get(shiftpos.getRow()).getCol().remove(shiftpos.getCol());
		fakeboard.getRow().get(shiftpos.getRow()).getCol().add(shiftpos.getCol(), fakeboard.getShiftCard());
		fakeboard.setShiftCard(newCard);
		return fakeboard;
	}

	//alle mögliche shifts 
	public static  PinToShift allpossiblesshifts(BoardData board) {
		List<PinToShift> newallshifts = new ArrayList<PinToShift>();
		BoardData mynewboard;
		List<PositionData> touteslescartes;
		PinToShift besteRowPin = new PinToShift();
		boolean trouvé =false;
		touslesshifts.setAllshifts(touslesshifts.removeforbiddenPos(verbotenpos));
		for (int i = 0; i <touslesshifts.getAllshifts().size(); i++) {
				mynewboard  = lefakeshift(touslesshifts.getAllshifts().get(i), board);	
				PositionData treasurepos =  TrouverlaPos(mynewboard,t);
				PositionData posdata = PindansBoard(mynewboard);
				touteslescartes = toutesLesCartes(mynewboard,posdata);
				if ( touteslescartes.size() > 1 && PositionDatafound(touteslescartes , treasurepos )==true ) {
					//listAff(allReachableCards);		
					PinToShift pinShift = MeilleurPosition(touteslescartes,treasurepos);
					pinShift.setShiftpos(touslesshifts.getAllshifts().get(i));
					newallshifts.add(pinShift);
					trouvé =true;
				}		
				}
		if(trouvé == true ) {
		besteRowPin =bestePosition(newallshifts);
		}else {
			besteRowPin.setShiftpos(RandomShift());	
			mynewboard = lefakeshift(besteRowPin.getShiftpos(),board);
			besteRowPin.setNewpinpos(step(PindansBoard(mynewboard),mynewboard));
		}
		return besteRowPin;	
	}
	
	public static  boolean[][] Visited_matrix(PositionData pinpos) {
		boolean[][] Visited_test = new boolean[7][7];
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 7; j++) {
				if (i == pinpos.getRow() && j == pinpos.getCol()) {
					Visited_test[i][j] = true;
				}
				Visited_test[i][j] = false;
			}
		}
		return Visited_test;
	}
	
	//get all the reachable cards in an ArrayList
	public static  List<PositionData> toutesLesCartes(BoardData board ,PositionData posdata ) {
		boolean Visited_test[][] = Visited_matrix(posdata);
		List<PositionData> allpositions=new ArrayList<PositionData>();
		List<PositionData> allpos = ReachablePositionsfinden(board, posdata.getRow(),
				posdata.getCol(), Visited_test, allpositions);
		return allpos;
	}
		
	//die Bewertungsfunktion
	public static  PinToShift MeilleurPosition(List<PositionData> allpos , PositionData treasurepos) {
		PinToShift bestOne = new PinToShift();
		int steps = 0 ;
		for (int i = 1; i < allpos.size(); i++) {
			if (allpos.get(i).getRow() == treasurepos.getRow() && allpos.get(i).getCol() == treasurepos.getCol()){
				PositionData NearestPosition = new PositionData();
				NearestPosition.setRow(allpos.get(i).getRow());
				NearestPosition.setCol(allpos.get(i).getCol());
				steps=i+1;
				bestOne.setNewpinpos(NearestPosition);
				bestOne.setSteps(steps);
				break;
			}
		}
		return bestOne;
	}
	//move message erzeugen
	public static  MoveMessageData createMoveMessage(CardData shiftcard, BoardData ServerBoard) {
		MoveMessageData move = new MoveMessageData();
		PinToShift allpins = allpossiblesshifts(ServerBoard);
		move.setNewPinPos(allpins.getNewpinpos());
		move.setShiftCard(shiftcard);
		move.setShiftPosition(allpins.getShiftpos());
		verbotenpos = verbotenePos(move.getShiftPosition());
		System.out.println("forbiddenpos Row :"+verbotenpos.getRow()+"forbiddenpos Col :"+verbotenpos.getCol());
		return move;
	}
	
	// die beste Pos finden
	public static PinToShift bestePosition(List<PinToShift> allpos ){
		PinToShift nearestPosition=allpos.get(0);
		for (int i = 1; i < allpos.size(); i++) {
			if (allpos.get(i).getSteps() < nearestPosition.getSteps()) {
				nearestPosition.setNewpinpos(allpos.get(i).getNewpinpos());
				nearestPosition.setShiftpos(allpos.get(i).getShiftpos());
			}
		}
		return nearestPosition;	
	}
	
	//Remove the unwanted shifts
	public static  PositionData verbotenePos(PositionData shiftPos){
		PositionData verboteneShiftpos = new PositionData();
		if (shiftPos.getCol() == 0) { // horizontal
			verboteneShiftpos.setCol(shiftPos.getCol()+6);
			verboteneShiftpos.setRow(shiftPos.getRow());
			
		}else if(shiftPos.getRow() == 0) {
			verboteneShiftpos.setCol(shiftPos.getCol());
			verboteneShiftpos.setRow(shiftPos.getRow()+6);
			
		}else if(shiftPos.getRow()== 6) {
			verboteneShiftpos.setCol(shiftPos.getCol());
			verboteneShiftpos.setRow(shiftPos.getRow()-6);
		}else {
			verboteneShiftpos.setCol(shiftPos.getCol()-6);
			verboteneShiftpos.setRow(shiftPos.getRow());
		}
		return verboteneShiftpos;
	}
		
	
	// der Player in board finden
	public static  PositionData PindansBoard(BoardData board) {
		PositionData posd = new PositionData();
		int k = 0;
		for(int i = 0 ; i < board.getRow().size();i++) {
			for(int j = 0 ;j<board.getRow().get(0).getCol().size();j++) {
				if(board.getRow().get(i).getCol().get(j).getPin().getPlayerID().size() > 0){
					for(int o = 0 ;o<board.getRow().get(i).getCol().get(j).getPin().getPlayerID().size();o++) {
					if(board.getRow().get(i).getCol().get(j).getPin().getPlayerID().get(o)==playerId){
					posd.setRow(i);
					posd.setCol(j);
					k=1;
					break;
					}
					}
				}			
			}
			if(k==1){
				break;
			}
		}
		return posd;
	}
	// sich ein Step bewegen ,falls keine possible 
    public static  PositionData step(PositionData PositionD ,BoardData board){
    	//System.out.println("Row :"+PositionD.getRow() +"Col :"+PositionD.getCol());
    	//PositionData Pos = new PositionData();
    	if(board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()).getOpenings().isRight()){
    		if(PositionD.getCol()< 6 &&board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()+1).getOpenings().isLeft()) {
    			PositionD.setCol(PositionD.getCol()+1);
    		}
    	}
    	else{
    			if(board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()).getOpenings().isBottom()){
    				if(PositionD.getRow()<6 && board.getRow().get(PositionD.getRow()+1).getCol().get(PositionD.getCol()).getOpenings().isTop()) {
        			PositionD.setRow(PositionD.getRow()+1);
    				}
    			}
    			else{
    			if(board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()).getOpenings().isLeft()){
    				if(PositionD.getCol()> 0 &&board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()-1).getOpenings().isRight()) {
        			PositionD.setCol(PositionD.getCol()-1);
    				}
    			else{
    				if(board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()).getOpenings().isTop()){
    					if(PositionD.getRow()> 0 &&board.getRow().get(PositionD.getRow()-1).getCol().get(PositionD.getCol()).getOpenings().isBottom()) {
    	    			PositionD.setRow(PositionD.getRow()-1);
    					}
    				}
    			}
	
    			}
    			}
    	}
    	
    	return PositionD;
    }
    
    // mein Algorithmus hat die Position des Treasures gefunden
    public static  boolean PositionDatafound(List<PositionData> allReachablepos , PositionData treasurepos){
    	boolean found = false;
    	for (int i = 0; i < allReachablepos.size(); i++) {
			if (allReachablepos.get(i).getRow() == treasurepos.getRow() && allReachablepos.get(i).getCol() == treasurepos.getCol()){
				found = true;
				break;
			}		
    }
    	return found;
    }	
    // ein Random Shift für shitf generieren
    public static  PositionData  RandomShift(){
    return touslesshifts.getAllshifts().get(randomNumber());
    }
    
    public static int randomNumber() {
	   return (int) (Math.random()*11);
   }
}

