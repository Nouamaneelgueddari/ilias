package de.fhac.mazenet.client.game;

import de.fhac.mazenet.server.generated.PositionData;

public class StepPos {
	PositionData Position;
	int steps;
	public StepPos(PositionData pos,int steps){
		Position = pos;
		this.steps=steps;
	}
	public PositionData getPosition() {
		return Position;
	}
	public void setPosition(PositionData position) {
		Position = position;
	}
	public int getSteps() {
		return steps;
	}
	public void setSteps(int steps) {
		this.steps = steps;
	}

}
